from lxml import etree

# citanie XML suborov bolo postavene na requests module - pri double checku zadania som si uvedomil ze je to 3rd party
# lib, ale nechal som to v kode ako moznost lebo je to prakticke rovno s citanim z URL. konstanta REMOTE governuje
# pouzitie remote alebo local xml suborov (na konci classy XMLReader)
REMOTE = False

if REMOTE:
    import requests

from typing import List

import os

class XMLReader(object):

    # ak produkt nema CODE element, a neexistuje ani VARIANTS, pripadne vo variante neexistuje CODE
    # potrebujem to nejak riesit - neviem ako, zadanie to nespecifikuje, dam aspon print do logu.
    @staticmethod
    def hanlde_missing_code(element: etree.ElementBase):
        message = "Found codeless element {0}".format(etree.tostring(element.text))
        # pre pripad ze by sme spracovanie takehoto suboru nechceli dokoncit, do exception handlingu mozem
        # dat napr. poslanie notifikacie adminovi o chybnom subore
        # raise CodelessProductException(message)

        # ratam s tym ze print output ide do logu
        print(message)

    # zo zadania mi nebolo jasne ci "ale zároveň sa nenachádzajú v súbore in1.xml a in2.xml." znamena ze sa musia
    # nachadzat v oboch suboroch aby boli vylucene z extra_products.csv, preto mam dve rozne metody ktore
    # vracaju dva rozne sety

    # metoda vracia set s CODEs nachadzajuce sa vo vsetkych suboroch zaroven (ak sa kod nenachadza v niektorom
    # zo suborov, nie je zahrnuty vo vratenom sete)
    @staticmethod
    def get_codes_in_all_files(urls: list) -> set:

        # zakladny check ci viem robit s poskytnutym parametrom to co potrebujem
        if type(urls) is not list or len(urls) == 0:
            raise Exception("Invalid argument urls: {0}".format(urls))
        # ak mam len jednu url, vratim rovno kody z nej
        if len(urls) == 1:
            return XMLReader.get_codes_from_files(urls)

        # cez XMLReader.get_codes_from_files stiahnem a vytvorim zvlast sety kodov pre kazdy zo suborov
        code_sets: List[set] = list(map(lambda url: XMLReader.get_codes_from_files([url]), urls))

        # nulty set pouzijem ako zaklad
        ret = code_sets[0]

        # a potom vytvaram sety obsahujuce "prieniky" kodov (nechavam iba kody ktore su vo vsetkych setoch)
        for i in range(1, len(code_sets)):
            ret = ret.intersection(code_sets[i])

        return ret

    # metoda vracia set s CODEs nachadzajucich sa v danych suboroch
    @staticmethod
    def get_codes_from_files(urls: list) -> set:

        # zakladny check ci viem robit s poskytnutym parametrom to co potrebujem
        if type(urls) is not list or len(urls) == 0:
            raise Exception("Invalid argument urls: {0}".format(type(urls)))

        codes = set()

        for url in urls:
            # zoberiem si etree z danej URL
            root = XMLReader.get_xml_etree(url)

            # iterujem dany SHOP
            for product in root:

                # najdem si CODE element v danom produkte
                code_elem: etree.ElementBase = product.find("CODE")

                # najdem si VARIANTS element v danom produkte
                variants_elem: etree.ElementBase = product.find("VARIANTS")

                # ak dany produkt nema CODE ani VARIANTS nieco je zle, a zapisem to do logu/poslem noticku
                # adminovi/zhodim skript/spustim DDOS na providera xml suboru (ospravedlnujem sa za hlupy vtip)
                if code_elem is None and variants_elem is None:
                    XMLReader.hanlde_missing_code(product)
                    continue

                # ak som nasiel CODE element, iba ho pridam do zoznamu kodov ktore vraciam
                if code_elem is not None:
                    codes.add(code_elem.text)
                else:
                    # inak sa snazim pridat zoznam kodov vsetkych variant z VARIANTS elementu
                    variant: etree.ElementBase
                    for variant in variants_elem:
                        variant_code_elem = variant.find("CODE")
                        # riesim eventualitu neexistujuceho kodu
                        if variant_code_elem is None:
                            XMLReader.hanlde_missing_code(product)
                            continue
                        # pridavam CODE variantu
                        codes.add(variant_code_elem.text)

        return codes

    # v tomto pripade mam koli prehladnosti format adresy pouzivajuci spolocny zaklad vsetkych poskytnutych URL/paths.
    # ci a ako moc je to (ne)prakticke by zalezalo na konkretnom IRL pripade

    URL_BASE = "https://data.brani.cz/tmp/backend-ukol/in{0}.xml"
    PATH_BASE = "xml/in{0}.xml"

    # precitam xml z daneho ciela a z obsahu inicializujem etree
    @staticmethod
    def get_xml_etree(url):
        file_content = XMLReader.get_xml_from_local_file(url) if not REMOTE else XMLReader.get_xml_from_url(url)
        return etree.fromstring(file_content)

    @staticmethod
    def get_xml_from_url(url):
        return requests.get(XMLReader.URL_BASE.format(url)).content

    @staticmethod
    def get_xml_from_local_file(filename):
        handle = open(os.path.join(os.path.dirname(__file__), XMLReader.PATH_BASE.format(filename)), 'rb')
        return handle.read()


# exception pre pripad neexistujuceho CODE elementu, ktoru by som vedel zvlast zachytavat keby riesime tuto eventualitu
class CodelessProductException(Exception):
    pass


