import os
import csv

from lxml import etree

from xmlreader import XMLReader


# dev funkcia na vytvorenie kontrolneho suboru ktory velmi ulahcuje vizualnu kontrolu toho, ci skript pracuje tak ako ma
# generuje CSV subor ktory obsahuje stlpec s kodmi v kazdom subore, resp. obsahuje prazdny cell pre
# kazdy kod ktory dany subor neobsahuje, a obsahuje ho niektory z ostatnych suborov - zoradene abecedne
def check_codes_file():

    filenames = ["-main", "1", "2"]

    # list setov kodov z kazdeho zo suborov, aby som ich vedel zobrazit jeden vedla druheho
    file_codes = list(map(lambda filename: XMLReader.get_codes_from_files([filename]), filenames))

    # otvorim subor na zapis a zapisem nazvy suborov do hlavicky
    handle = open("{0}\check.csv".format(os.getcwd()), "w", encoding="utf-8")
    writer = csv.writer(handle, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL, lineterminator='\n')
    writer.writerow(filenames)

    # prvy set skonvertujem na list aby som ho mohol abecedne zoradit
    main_list = list(file_codes[0])
    main_list.sort()

    # zapisujem kody z main suboru a ich vyskyt alebo prazdne stringy v ostatnych suboroch line by line
    # iterujem iba kody z main suboru, kody ktore sa nenachadzaju v in-main.xml ma nezaujimaju
    for code in main_list:
        writer.writerow((code, code if code in file_codes[1] else "", code if code in file_codes[2] else ""))

    handle.close()

# hlavna funkcionalita skriptu
def main():

    # kody na vyradenie z produktov v in-main.xml,
    #
    # >>>> nachadzajuce sa v subore in1.xml ALEBO in2.xml <<<<
    exclude = XMLReader.get_codes_from_files(["1", "2"])

    # zo zadania mi nebolo jasne ci "ale zároveň sa nenachádzajú v súbore in1.xml a in2.xml." znamena ze sa musia
    # nachadzat v oboch suboroch aby boli vylucene z extra_products.csv, alebo nie. na to sluzi extra metoda vracia kody
    #
    # >>>> nachadzajuce sa v subore in1.xml A ZAROVEN in2.xml <<<<
    # exclude = XMLReader.get_codes_in_all_files(["1", "2"])

    # otvorenie suboru na zapis
    handle = open("{0}\extra_products.csv".format(os.getcwd()), "w", encoding="utf-8")
    csv_writer = csv.writer(handle, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL, lineterminator='\n')
    # hlavicka
    csv_writer.writerow(("code", "name"))

    # etree XML suboru z ktoreho chcem filtrovany vystup
    root = XMLReader.get_xml_etree("-main")

    # iterujem etree z main suboru
    for child in root:

        # hladam CODE element
        code_element: etree.ElementBase = child.find("CODE")

        # ak existuje a nie je v zozname vyradenych kodov z in1.xml a in2.xml, zapisujem do extra_products
        if code_element is not None:
            if code_element.text not in exclude:
                csv_writer.writerow((code_element.text, child.find("NAME").text))
        # ak CODE element neexistuje, a neexistuje ani VARIANTS potrebujem to nejak riesit - neviem ako, zadanie
        # to nespecifikuje, dam aspon print do logu
        elif child.find("VARIANTS") is None:
            XMLReader.hanlde_missing_code(child)
        # ak neexistuje CODE element, hladam kody vo variantoch
        else:
            variant: etree.ElementBase
            for variant in child.find("VARIANTS"):

                code_element = variant.find("CODE")
                # riesim pripad ak neexistuje CODE vo variante
                if code_element is None:
                    XMLReader.hanlde_missing_code(variant)
                    continue

                # ak je CODE variantu vo vyradenych kodoch, pokracujem dalej na dalsi variant/produkt
                if code_element.text in exclude:
                    continue

                # hladam nazov variantu v parametroch
                parameters_element: etree.ElementBase = variant.find("PARAMETERS")

                name_param_element = None
                # pouzivam generator na najdenie paru elementov NAME / VALUE v PARAMETERS
                # generatory povazujem za docela prehladnu formu zapisu napr aj na vyhladavanie prvku v liste
                # podla parametrov, ale uz som pocul aj o tom ze onelinery na tento sposob niektorym koderom nevyhovuju.
                # treba mi dat vediet co pouzivat, viem to dat aj do nejakeho for loopu/get_x_from_y_by_z funkcie
                try:
                    name_param_element = \
                            next(parameter for parameter in parameters_element
                             if parameter.find("NAME") is not None
                             and parameter.find("VALUE") is not None
                             and parameter.find("NAME").text == "Varianta")
                # ak je generator prazdny, next vracia StopIteration, a musim ho catchnut
                except StopIteration:
                    pass

                # meno v extra_products pouzivam podla toho, ci som nasiel nazov variantu v parametroch alebo nie
                # ak nie, pouzivam meno produktu
                name = name_param_element.find("VALUE").text if name_param_element is not None else child.find("NAME").text

                csv_writer.writerow((code_element.text, name))

    handle.close()


check_codes_file()
main()

