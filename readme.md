### Dependencies:
 - lxml

### Zopár poznámok 

Remote verzia používa libku `requests` 

> `pip install requests`

To že je to 3rd party lib a teda v rozpore so zadaním som si uvedomil keď som začal písať písať dependencies v tomto doce. Kvôli tomu je v xmlreader.py konštanta REMOTE, ktorá hovorí o tom či sa `requests` importuje alebo nie, a následne kontroluje odkiaľ sa xml súbory budú čítať. (Priamo z poskytnutých URL cez `requests` v prípade REMOTE=True, inak z lokálnych súborov. Defaultne je to nastavené na False - v súlade so zadaním.)

V zadaní mi vznikla jedna nejasnosť 
 > ... ale zároveň sa nenachádzajú v súbore in1.xml a in2.xml.
 
V Slovenčine sa spojka 'a' používa trochu inak ako v logike, a táto veta môže podla mňa znamenať dve rôzne veci nakolko ide o zadanie pre programátora.

Kvôli tomu som urobil obe verzie: bol by som sa spýtal na význam, ale k tomuto konkrétnemu problému som sa dostal o siedmej ráno, a riešenie mi trvalo podla togglu všehovšudy 12 minút. Tipol som si že o siedmej ráno nebudeš k dispo na debaty (Brani), tak som to vyriešil dvoma verziami funkcie. Bližšie je to popísané v main.py:38-46



